import boto3
import io
from PIL import Image, ImageDraw, ExifTags, ImageColor



def filterHappyIndex(emotions):
    

    if(emotions['Type'] == "HAPPY"):
        return True
    else:
        return False

def getHappyIndex(s):
    return list(filter(filterHappyIndex, s['Emotions']))[0]

def print_iterator(it):
    for x in it:
        print(x['Confidence'], end=' ')
    print('')  # for new line

def show_faces(photo,bucket):
     

    client=boto3.client('rekognition')

    # Load image from S3 bucket
    s3_connection = boto3.resource('s3')
    s3_object = s3_connection.Object(bucket,photo)
    s3_response = s3_object.get()

    stream = io.BytesIO(s3_response['Body'].read())
    image=Image.open(stream)
    
    #Call DetectFaces 
    response = client.detect_faces(Image={'S3Object': {'Bucket': bucket, 'Name': photo}},
        Attributes=['ALL'])

    imgWidth, imgHeight = image.size  
    draw = ImageDraw.Draw(image)  
                    

    # calculate and display bounding boxes for each detected face       
    print('Detected faces for ' + photo)    

    #happyfaces  = filter(filterHappyIndex,response['FaceDetails'][]
    map_iterator = map(getHappyIndex, response['FaceDetails'])
    print(type(map_iterator))
    listH = list(map_iterator)
    print(listH)
    happiestIdx = listH[0]['Confidence']
    print('Happiest Index', happiestIdx)
    #print_iterator(map_iterator)

    numMale = 0
    numFemale = 0
    idx = 0
    for faceDetail in response['FaceDetails']:
        print('The detected face is between ' + str(faceDetail['AgeRange']['Low']) 
              + ' and ' + str(faceDetail['AgeRange']['High']) + ' years old')
        
        if str(faceDetail['Gender']['Value']) == "Male":
            numMale = numMale + 1
        if str(faceDetail['Gender']['Value']) == "Female":
            numFemale = numFemale + 1
        box = faceDetail['BoundingBox']
        left = imgWidth * box['Left']
        top = imgHeight * box['Top']
        width = imgWidth * box['Width']
        height = imgHeight * box['Height']
                

        print('Left: ' + '{0:.0f}'.format(left))
        print('Top: ' + '{0:.0f}'.format(top))
        print('Face Width: ' + "{0:.0f}".format(width))
        print('Face Height: ' + "{0:.0f}".format(height))

        points = (
            (left,top),
            (left + width, top),
            (left + width, top + height),
            (left , top + height),
            (left, top)

        )
        if (idx ==0):
            draw.line(points, fill='#f0d400', width=2)
        else:
            draw.line(points, fill='#00d400', width=2)

        idx = idx+1
        # Alternatively can draw rectangle. However you can't set line width.
        #draw.rectangle([left,top, left + width, top + height], outline='#00d400') 

    image.show()
    print("Number of Male: ", numMale)
    print("Number of Female: ", numFemale)
    return len(response['FaceDetails'])


def main():
    bucket="docdoc-test"
    photo="people.jpg"

    faces_count=show_faces(photo,bucket)
    print("faces detected: " + str(faces_count))


if __name__ == "__main__":
    main()